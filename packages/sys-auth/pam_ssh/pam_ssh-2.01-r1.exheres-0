# Copyright 2009 Jonathan Dahan <jedahan@gmail.com>
# Copyright 2011 Ali Polatel <alip@exherbo.org>
# Based in part upon pam_ssh-1.97.ebuild which is
#   Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

require flag-o-matic pam
require sourceforge [ project=pam-ssh suffix=tar.xz ]

SUMMARY="Uses ssh-agent for a single sign-on"
DESCRIPTION="This PAM module provides single sign-on behavior for SSH. The user
types an SSH passphrase when logging in (probably to GDM, KDM, or XDM) and is
authenticated if the passphrase successfully decrypts the user's SSH private
key. In the PAM session phase, an ssh-agent process is started and keys are
added. For the entire session, the user can SSH to other hosts that accept key
authentication without typing any passwords."

LICENCES="BSD-2 as-is"
PLATFORMS="~amd64 ~x86"
SLOT="0"
BUGS_TO="alip@exherbo.org"

DEPENDENCIES="
    net-misc/openssh
    sys-libs/pam
    providers:libressl? ( dev-libs/libressl:= )
    providers:openssl? ( dev-libs/openssl )
"

DEFAULT_SRC_CONFIGURE_PARAMS=( --with-pam-dir=$(getpam_mod_dir) )

src_configure() {
    # Don't export symbols which may clash with other software.
    # Gentoo bug #274924
    append-ldflags -Wl,--version-script="${FILES}"/pam_symbols.ver

    default
}

