# Copyright 2009 Ali Polatel <alip@exherbo.org>
# Copyright 2014 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require setup-py [ import=setuptools with_opt=true option_name="html-report" multibuild=false ]
require github [ user=danmar ] qmake

export_exlib_phases src_prepare src_configure src_compile src_test src_install

SUMMARY="A tool for static C/C++ code analysis"
DESCRIPTION="
Cppcheck is versatile. You can check non-standard code that includes various
compiler extensions, inline assembly code, etc.
The goal is no false positives.
"
HOMEPAGE+=" http://cppcheck.sourceforge.net/"

BUGS_TO="alip@exherbo.org"

LICENCES="GPL-3"
SLOT="0"
MYOPTIONS="
    html-report [[ description = [ Installs a tool to generate HTML reports of cppcheck's results ] ]]
    qt5 [[ description = [ Builds a Qt5-based GUI for cppcheck ] ]]
"

# The Cppcheck binary was compiled with CFGDIR set to \"/usr/share/cppcheck/cfg\"
RESTRICT="test"

DEPENDENCIES="
    build:
        qt5? (
            x11-libs/qttools:5 [[ note = [ lrelease/lconvert for translations ] ]]
        )
    build+run:
        qt5? ( x11-libs/qtbase:5 )
    run:
        html-report? ( dev-python/Pygments[python_abis:*(-)?] )
"

DEFAULT_SRC_COMPILE_PARAMS=( CC="${CC}" CXXFLAGS="${CXXFLAGS}" )

DEFAULT_SRC_INSTALL_PARAMS=( BIN="${IMAGE}"/usr/$(exhost --target)/bin )

EQMAKE_SOURCES=( gui.pro )

cppcheck_src_prepare() {
    default

    # Don't install cppcheck-htmlreport unconditionally
    edo sed -e "/install htmlreport\/cppcheck-htmlreport/d" \
            -i Makefile

    if option qt5 ; then
        edo lrelease-qt5 gui/${PN}_*.ts

        # fix multiarch search path for Qt translations
        edo sed \
            -e 's:datadir = appPath:datadir = "/usr/share/cppcheck":g' \
            -i gui/translationhandler.cpp
    fi
}

cppcheck_src_configure() {
    default

    if option html-report ; then
        edo pushd htmlreport
        setup-py_src_configure
        edo popd
    fi

    if option qt5 ; then
        edo pushd gui
        eqmake 5
        edo popd
    fi
}

cppcheck_src_compile() {
    emake CFGDIR="/usr/share/${PN}/cfg"

    if option html-report ; then
        edo pushd htmlreport
        setup-py_src_compile
        edo popd
    fi

    if option qt5 ; then
        edo pushd gui
        emake
        edo popd
    fi
}

cppcheck_src_test() {
    # Don't run setup-py_src_test
    default
}

cppcheck_src_install() {
    default

    insinto /usr/share/${PN}/cfg
    doins cfg/*.cfg

    if option html-report ; then
        edo pushd htmlreport
        setup-py_src_install
        edo popd
    fi

    if option qt5 ; then
        dobin gui/${PN}-gui
        dodoc gui/{projectfile.txt,gui.cppcheck}
        insinto /usr/share/cppcheck/lang
        doins gui/*.qm
        insinto /usr/share/applications
        doins gui/${PN}-gui.desktop
        insinto /usr/share/icons/hicolor/scalable/apps
        doins gui/${PN}-gui.svg
    fi
}

