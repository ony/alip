# Copyright 2009 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require lua [ multiunpack=true whitelist="5.1 5.2" ]

SUMMARY="C extension module for Lua 5.1 which adds bitwise operations on numbers. "
DESCRIPTION="
Features:
# Supported functions: bit.tobit, bit.tohex, bit.bnot, bit.band, bit.bor,
bit.bxor, bit.lshift, bit.rshift, bit.arshift, bit.rol, bit.ror, bit.bswap
# Consistent semantics across 16, 32 and 64 bit platforms.
# Supports different lua_Number types: either IEEE 754 doubles, int32_t or int64_t.
# Internal self-test on startup to detect miscompiles. Includes a comprehensive test and benchmark suite.
# Compatible with the bitwise operations in the upcoming LuaJIT 2.x.
# It's as fast as you can get with the standard Lua/C API.
"
HOMEPAGE="http://bitop.luajit.org/"
DOWNLOADS="http://bitop.luajit.org/download/${PNV}.tar.gz"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES=""

DEFAULT_SRC_COMPILE_PARAMS=( CC=${CC} )

prepare_one_multibuild() {
    # Respect CFLAGS and fix include path
    edo sed -i \
        -e "/^CFLAGS/s%-O2 -fomit-frame-pointer%${CFLAGS}%" \
        -e "/^INCLUDES/s/.*/INCLUDES= -I \/usr\/$(exhost --target)\/include\/lua$(lua_get_abi)/" \
        Makefile
}

test_one_multibuild() {
    emake LUA=lua$(lua_get_abi) test
}

install_one_multibuild() {
    lua_install_cmodule bit.so
    emagicdocs
}

